"""a module that houses various population models useful for generating synthetic GW events and general population inference
"""

from __future__ import print_function

__author__ = "Chris Pankow <'chris.pankow@ligo.org'>, Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

import os
from shutil import move ### used to make file writing atomic

import numpy as np
import h5py

import lal

from .event import Event

#-------------------------------------------------
# I/O associated with lists of Events
#-------------------------------------------------

def tmppath(path):
    return os.path.join(os.path.dirname(path), '.'+os.path.basename(path))

#------------------------
### ascii
#------------------------

def _events2array(events, attrs):
    attrs = [(attr, 'S50' if attr=='approximant' else 'float') for attr in attrs]
    return np.array([tuple(getattr(event, attr) for attr, _ in attrs) for event in events], dtype=attrs)

def events2csv(events, attrs, path, delimiter=','):
    """write samples out to a csv file
    """
    samples = _events2array(events, attrs)

    ### write the result to a temporary location
    names = samples.dtype.names
    fmt = delimiter.join('%s' if name=='approximant' else '%.18e' for name in names) ### NOTE, includes delimiter
    tmp = tmppath(path)
    np.savetxt(tmp, samples, comments='', header=','.join(names), fmt=fmt)

    ### move to final location
    move(tmp, path)

def csv2events(path, delimiter=','):
    """Load a set of events generated from an ASCI table with a header. This is accomplished by an internal generator method which 'replays' the generated events from the file.
    """
    data = np.genfromtxt(path, names=True, delimiter=delimiter)

    attrs = tuple(data.dtype.names)
    events = []
    for sample in data:
        event = Event()
        for attr in attrs:
            setattr(event, attr, sample[attr])

    return events

#------------------------
### xml
#------------------------

def events2xml(events, attrs, path):
    """Dump the current event set to a LIGOLW XML file.
    """
    _mapping = {
        "longitude": "right_ascension",
        "latitude": "declination"
    }

    from glue.ligolw import lsctables, utils, ligolw, ilwd
    from glue.ligolw.utils import process as lw_process
    lsctables.use_in(ligolw.LIGOLWContentHandler)

    xmldoc = ligolw.Document()
    xmldoc.appendChild(ligolw.LIGO_LW())

    proc_table = lw_process.register_to_xmldoc(xmldoc, __name__, {})
    proc_id = proc_table.process_id

    dump_attrs = set(lsctables.SimInspiralTable.validcolumns.keys()) & attrs
    dump_attrs = list(dump_attrs)
    dump_attrs += _mapping.keys()
    addl_attrs = ["simulation_id", "process_id", "geocent_end_time", "geocent_end_time_ns"]
    sim_table = lsctables.New(lsctables.SimInspiralTable, dump_attrs + addl_attrs)
    for event in events:
        row = sim_table.RowType()
        row.process_id = proc_id
        row.simulation_id = sim_table.get_next_id()
        if hasattr(event, 'time'):
            _t = lal.LIGOTimeGPS(event.time)
            row.geocent_end_time = _t.gpsSeconds
            row.geocent_end_time_ns = _t.gpsNanoSeconds

        for a in dump_attrs:
            if a in _mapping:
                setattr(row, a, getattr(event, _mapping[a]))
            else:
                setattr(row, a, getattr(event, a))
        sim_table.append(row)

    xmldoc.childNodes[0].appendChild(sim_table)

    ### write to a temporary path
    tmp = tmppath(path)
    utils.write_filename(xmldoc, tmp, gz=tmp.endswith("gz"))

    ### move to final location
    move(tmp, path)

def xml2events(path):
    """Load a set of events from a LIGOLW XML file. Must contain one and only one sim table.
    FIXME: Should use "replay" strategy a la the HDF5 version.
    """
    from glue.ligolw import lsctables, utils, ligolw
    lsctables.use_in(ligolw.LIGOLWContentHandler)

    xmldoc = utils.load_filename(path, contenthandler=ligolw.LIGOLWContentHandler)
    tbl = lsctables.SimInspiralTable.get_table(xmldoc)
    attrs = set(tbl.validcolumns.keys())

    events = []
    for xmlevent in tbl:
        event = Event()
        for k in list(attrs):
            try:
                setattr(event, k, getattr(xmlevent, k))
            except AttributeError:
                # FIXME: on the off chance we get heterogenous data, this
                # will prevent this attribute from being retrieved later
                attrs.remove(k)
                continue
        event.eccentricity = 0. ### FIXME: don't hard-code this?
        event.right_ascension = event.longitude
        event.declination = event.latitude
        event.time = event.geocent_end_time + 1e-9 * event.geocent_end_time_ns

        events.append(event)

#------------------------
### hdf5
#------------------------

def events2hdf5(events, attrs, path, root=None, meta=None):
    tmp = tmppath(path)
    with h5py.File(tmp, "a") as hfile:
        try:
            grp = hfile[root]
        except:
            grp = hfile.create_group(root)

        if meta is not None:
            for k, v in meta.items():
                grp.attrs[k] = v

        ### create one big dataset, which assume homogenous data
        grp.create_dataset('events', data=_events2array(events, attrs))

    ### move to final location
    move(tmp, path)

def hdf52events(path, root=None):
    """Load a set of events generated and serialized into HDF5 from a previous instance of an EventGenerator. This will reinitialize any of the fields that were saved in the previous generation run (via checking the Dataset names).
    This is accomplished by an internal generator method which 'replays' the generated events from the HDF5 file. This may be slower than you were hoping, and there's no way to recovery the individual sets of parameters or their generator functions from the previous run.
    """
    events = []
    with h5py.File(path, "r") as hfile:
        grp = hfile[root or "/events/"]
        data = grp['events'][...] ### copy everything out of the file

    attrs = data.dtype.names
    for sample in data:
        event = Event()
        for attr in attrs:
            setattr(event, attr, sample[attr])

    return events

#------------------------
### pandas dataframe
#------------------------

def events2dataframe(events, attrs):
    from pandas import DataFrame
    return DataFrame.from_records(list(map(dict, [[(attr, getattr(e, attr)) for attr in attrs] for e in events])))
