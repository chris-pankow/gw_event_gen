"""a module that houses various population models useful for generating synthetic GW events and general population inference
"""

from __future__ import print_function

__author__ = "Chris Pankow <'chris.pankow@ligo.org'>, Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

import numpy as np

import scipy
from scipy.integrate import cumtrapz
from scipy.interpolate import interp1d
from scipy.stats import gaussian_kde

#---------------------------------------------------------------------------------------------------
# basic utilities
#---------------------------------------------------------------------------------------------------

def subclasses(klass):
    ans = dict()
    for obj in klass.__subclasses__():
        ans[obj.__name__] = obj
        ans.update(subclasses(obj))
    return ans

#---------------------------------------------------------------------------------------------------
# reference calculations
#---------------------------------------------------------------------------------------------------

# Redshift distributions derived from SFRs
def _madau_dickinson_sfr(z, coef=0.015, num_exp=2.7, den_coef=1/2.9, den_exp=5.6):
    return coef * (1 + z)**num_exp / (1 + (den_coef * (1 + z))**den_exp)

# Form CDF over complete range of "validity"
_md_zvals = np.linspace(0, 10, 1000)
# CDF
_md_cdf = cumtrapz(_madau_dickinson_sfr(_md_zvals), _md_zvals)
# Interpolated inverse CDF
_md_invcdf = interp1d(_md_cdf, _md_zvals[:-1])
# Interpolated CDF
_md_cdf = interp1d(_md_zvals[:-1], _md_cdf)

# This is to hack around the painfully slow cosmology calculator from astropy
def init_cosmology(min_log10_z=-3, max_log10_z=1):
    """Damn astropy, you slow.
    """
    from astropy.cosmology import Planck15
    _com_z_cache = np.logspace(min_log10_z, max_log10_z, 1000)
    _com_z_cache = np.concatenate(([0], _com_z_cache))
    _com_cache = Planck15.comoving_volume(_com_z_cache).value

    _inv_com_cache = interp1d(_com_cache, _com_z_cache)
    com_v = interp1d(_com_z_cache, _com_cache)
    _com_d_cache = Planck15.comoving_distance(_com_z_cache).value
    com_d = lambda z: _com_cache[np.searchsorted(_com_d_cache, z)]
    return com_v, com_d, _inv_com_cache

# NOTE: One will have to recall this idiom to reset the internal interpolators
_com, _com_dist, _inv_com = init_cosmology()

#---------------------------------------------------------------------------------------------------
# Distributions not in scipy
#---------------------------------------------------------------------------------------------------
def negative_power(a, b, idx, size=1):
    if a==b: ### this is effectively a delta-function
        return a*np.ones(size, dtype=float)

    if idx < 0:
        assert b > a > 0
    else:
        assert b > a

    if idx == -1:
        norm = b / a
        rvs = np.random.uniform(0, 1, size=size)
        return norm**rvs * a

    norm = b**(idx+1) - a**(idx+1)
    rvs = np.random.uniform(0, 1, size=size)
    # cdf = (rvs**(idx+1) - a**(idx+1)) / norm
    return (norm * rvs + a**(idx+1))**(1.0/(idx+1))

#---------------------------------------------------------------------------------------------------
# Attribute generators
#---------------------------------------------------------------------------------------------------

class SamplingDistribution(object):
    """a thin wrapper around some functionality to compute basic attributes of distributions
    """
    _variates = ()
    _params = ()
    _required = ()

    def __init__(self):
        self._values = np.array([], dtype=float)

    @property
    def params(self):
        return self._values

    @params.setter
    def params(self, new):
        assert len(new)==len(self._values)
        self._values[:] = new
        self._update() ### call this in case there are special things that need to be calculated when we update paramters

    def _update(self):
        pass ### default is to do nothing because there are no special functions. May be overridden as needed by children

    @staticmethod
    def _check_variates(variates, args):
        assert len(args) == len(variates), 'must supply exactly %d required parameters as arguments! args=%s'(len(variates), ', '.join(variates))

    def rvs(self, size=1, *args):
        """draw a specified number of realizations from this distribution
        """
        self._check_variates(self._required, args)
        return self._rvs(size, *args)

    def _rvs(self, size=1, *required):
        raise NotImplementedError('child classes should override this')

    def pdf(self, *args):
        """return the value of the distribution at the specified point
        signature should be pdf(self, *variates, *required)
        """
        self._check_variates(self._variates+self._required, args)
        return self._pdf(*args)

    def _pdf(self, *args):
        raise NotImplementedError('child classes should override this')

    def jacobian(self, *args):
        """return the jacobian of the distribution at the specified point
        signature should be jacobian(self, *variates, *required)
        """
        self._check_variates(self._variates+self._required, args)
        return self._jacobian(*args)

    def _jacobian(self, *args):
        raise NotImplementedError('child classes should override this')

    def hessian(self, *args):
        """return the hessian of the distribution at the specified point
        signature should be hessian(self, *variates, *required)
        """
        self._check_variates(self._variates+self._required, args)
        return self._hessian(*args)

    def _hessian(self, *args):
        raise NotImplementedError('child classes should override this')

#-------------------------------------------------

class ExternalDistribution(SamplingDistribution):
    """a child class that imports samples from an external source and provides samples based on these
    """
    
    def __init__(self, path=None, **columns):
        """columns kwargs are maps from the variates (keys) to the column names in path (values)
        """
        if path is None:
            raise ValueError('please supply a path to the external samples!')
        self._path = path
        self._variates = tuple(sorted(columns.keys())) ### store the variates
        self._data = self.load(path, [columns[variate] for variate in self._variates])

    @staticmethod
    def load(path, variates):
        if path.endswith('csv'):
            data = np.genfromtxt(path, names=True, delimiter=',')
        else:
            data = np.genfromtxt(path, names=True)
        return np.array([data[variate] for variate in variates], dtype=float).transpose() ### only remember the things we care about

    def _rvs(self, size=1, *args):
        ans = np.empty((size, len(self._variates)), dtype=float)
        for i, j in enumerate(np.random.randint(len(self._data), size=size)):
            ans[i,:] = self._data[j,:]
        return ans

    def _pdf(self, *args):
        if np.any(np.prod(arg==self._data[:,i]) for i, arg in enumerate(args)):
            return 1.
        else:
            return 0.

    def _jacobian(self, *args):
        return np.array([], dtype=float)

    def _hessian(self, *args):
        return np.array([[]], dtype=float)

DEFAULT_WHITENED_BANDWIDTH = 0.1
class ExternalKDEDistribution(ExternalDistribution):
    """a child class that imports samples from an external source and provides samples based on a Kernel Density Estimate constructed with these
    """

    def __init__(self, path=None, whitened_bandwidth=DEFAULT_WHITENED_BANDWIDTH, weight_column=None, **columns):
        ExternalDistribution.__init__(self, path=path, **columns)
        if weight_column is None:
            self._weights = np.ones(len(self._data), dtype=float)/len(self._data)
        else:
            self._weights = self.load(self._path, [weight_column])
            self._weights /= np.sum(self._weights)

        self._whitener_params = [self.build_whitener(self._data[:,i], self._weights) for i in range(len(self._variates))]

        ### update data to be whitened data
        for i, params in enumerate(self._whitener_params):
            self._data[:,i] = self.whiten(self._data[:,i], *params)

        ### build the kde
        self._kde = self.build_kde(
            self._data,
            self._weights,
            bandwidth=whitened_bandwidth,
        )

    @staticmethod
    def build_whitener(data, weights):
        norm = np.sum(weights)
        m1 = np.sum(data * weights) / norm
        m2 = np.sum(data**2 * weights) / norm
        return m1, (m2 - m1**2)**0.5

    @staticmethod
    def whiten(data, mean, stdv):
        return (data - mean) / stdv

    @staticmethod
    def color(data, mean, stdv):
        return (data * stdv) + mean

    @staticmethod
    def build_kde(data, weights, bandwidth=DEFAULT_WHITENED_BANDWIDTH):
        if scipy.__version__ < '1.3.0':
            if np.any(weights[0]!=weights):
                raise NotImplementedError('scipy.stats.gaussian_kde only supports non-trivial weights with __version__>=1.3.0')
            return gaussian_kde(np.transpose(data), bw_method=bandwidth)
        return gaussian_kde(np.transpose(data), weights=weights, bw_method=bandwidth)

    def _rvs(self, size=1, *args):
        ans = self._kde.resample(size=size) ### sample in the whitened data
        ans = ans.transpose()

        for i, params in enumerate(self._whitener_params): ### color the result
            ans[:,i] = self.color(ans[:,i], *params)
        return ans

    def _pdf(self, *args):
        # just evaluate the kde at the whitened args
        return self._kde.pdf(*[self.whiten(arg, *params) for arg, params in zip(args, self._whitener_params)])

#-------------------------------------------------

class TimeDistribution(SamplingDistribution):
    """a child class used to declare things are distributions over time
    """
    _variates = ("time",)

class EccentricityDistribution(SamplingDistribution):
    """a child class used to declar things are distributions over eccentricity
    """
    _variates = ("eccentricity",)

class OrientationDistribution(SamplingDistribution):
    """a child class used to delcare things are distributions over orientation
    """
    _variates = ("right_ascension", "declination", "inclination", "polarization", "coa_phase")

class DistanceDistribution(SamplingDistribution):
    """a child class used to declare things are distance distributions
    """
    _variates = ("distance",)

class RedshiftDistribution(SamplingDistribution):
    """a child class used to declare things are redshift distributions
    """
    _variates = ("z",)

class MassDistribution(SamplingDistribution):
    """a child class used to declare things are mass distributions
    """
    _variates = ("mass1", "mass2")

class SpinDistribution(SamplingDistribution):
    """a child class used to declare things are spin distributions
    """
    _variates = ("spin1x", "spin1y", "spin1z", "spin2x", "spin2y", "spin2z")

#-------------------------------------------------
# Extrinsic parameters
#-------------------------------------------------

class UniformEventTime(TimeDistribution):
    """Uniform event time (seconds) over a year (arbitrarily defined).
    """
    _params = ('t0', 'dur')

    def __init__(self, t0=1e9, dur=86400*365):
        self._values = np.array([t0, dur], dtype=float)

    @property
    def t0(self):
        return self._values[0]

    @t0.setter
    def t0(self, new):
        self._values[0] = new

    @property
    def dur(self):
        return self._values[1]

    @dur.setter
    def dur(self, new):
        self._values[1] = new

    def _rvs(self, size=1):
        return self.t0 + np.random.random((size, 1))*self.dur

    def _pdf(self, t):
        return float((self.t0<=t)*(t<self.t0+self.dur))/self.dur

    def _jacobian(self, t):
        if self.pdf(t) > 0:
            return np.array((0., -1./self.dur**2), dtype=float)
        else:
            return np.array((0., 0.), dtype=float)

    def _hessian(self, t):
        if self.pdf(t) > 0:
            return np.array(((0., 0.), (0., 1./self.dur**3)), dtype=float)
        else:
            return np.zeros((2,2), dtype=float)

### Source eccentricity generators
class ZeroEccentricity(EccentricityDistribution):
    """zero eccentricity
    """
    _params = ()

    def _rvs(self, size=1):
        return np.zeros((size, 1), dtype=float)

    def _pdf(self, e):
        if e==0:
            return 1.
        else:
            return 0.

    def _jacobian(self, e):
        return np.array([], dtype=float)

    def _hessian(self, e):
        return np.array([[]], dtype=float)

### Source orientation generators
class RandomOrientation(OrientationDistribution):
    """random event orientation
    """
    _parmas = ()

    def _rvs(self, size=1):
        ra = np.random.uniform(0, 2 * np.pi, size=size)
        dec = np.arcsin(np.random.uniform(-1, 1, size=size))
        incl = np.arccos(np.random.uniform(-1, 1, size=size))
        psi = np.random.uniform(0, np.pi, size=size)
        coa_phase = np.random.uniform(0, 2 * np.pi, size=size)
        return np.array(zip(ra, dec, incl, psi, coa_phase), dtype=float)

    def _pdf(self, ra, dec, incl, psi, coa_pahse):
        ans = 1.
        ans *= (2*np.pi)**-1   ### normalizatoin from ra
        ans *= np.cos(dec)/2   ### dec
        ans *= np.sin(incl)/2  ### incl
        ans *= np.pi**-1       ### psi
        ans *= (2*np.pi)**-1   ### coa_phase
        return ans

    def _jacobian(self, ra, dec, incl, psi, coa_phase):
        return np.array([], dtype=float)

    def _hessian(self, ra, dec, incl, psi, coa_phase):
        return np.array([[]], dtype=float)

class OptimallyOriented(OrientationDistribution):
    """assumes the detector arms are aligned with the coordinate system, so zenith is at RA=0, Dec=pi/2
    """
    _params = ()

    def _rvs(self, size=1):
        # Inclination, coa_phase (ignored), polarization (ignored)
        ans = np.zeros((size, len(self._variates)), dtype=float)
        ans[:,1] = np.pi/2
        return ans

    def _pdf(self, ra, dec, incl, psi, coa_pahse):
        if (ra==0) and (dec==np.pi/2) and (incl==0) and (psi==0) and (coa_phase==0):
            return 1.
        else:
            return 0.

    def _jacobian(self, ra, dec, incl, psi, coa_pahse):
        return np.array([], dtype=float)

    def _hessian(self, ra, dec, incl, psi, coa_pahse):
        return np.array([[]], dtype=float)

### Redshift generators
class MadauDickinsonRedshift(RedshiftDistribution):
    """Obtain redshift distributed as the SFR inferred from Madau and Dickinson (2014), eqn 15.
    """
    _params = ('z_min', 'z_max')

    def __init__(self, z_min=0, z_max=10):
        assert z_min < z_max
        if z_max >= _md_zvals[-1]:
            raise ValueError("Invalid maximum redshift (z={0:f}), MD SFR only valid up to z ~ 8-10.".format(z_max))
        self._values = np.array([z_min, z_max], dtype=float)

    @property
    def z_min(self):
        return self._values[0]

    @z_min.setter
    def z_min(self, new):
        self._values[0] = new

    @property
    def z_max(self):
        return self._values[1]

    @z_max.setter
    def z_max(self, new):
        self._values[1] = new

    def _rvs(self, size=1):
        n1 = _md_cdf(self._z_max)
        n2 = _md_cdf(self._z_min)
        rv = np.random.uniform(n1, n2, size=(size, 1))
        return _md_invcdf(rv)

    def _pdf(self, z):
        return _madau_dickinson_sfr(z) / (_md_cdf(self._z_max) - _md_cdf(self._z_min))

    def _jacobian(self, z):
        base = _madau_dickinson_sfr(z) / (_md_cdf(self._z_max) - _md_cdf(self._z_min))**2
        return np.array((
                +base * _madau_dickinson_sfr(self.z_min),
                -base * _madau_dickinson_sfr(self.z_max)
            ),
            dtype=float
        )

    def _hessian(self, z):
        raise NotImplementedError('''need access to the second derivative of the SFR''')

class UniformLogRedshift(RedshiftDistribution):
    """uniform in log(1+z)
    """
    _params = ('z_min', 'z_max')

    def __init__(self, z_min=0, z_max=10):
        assert z_min < z_max
        self._values = np.array([z_min, z_max], dtype=float)

    @property
    def z_min(self):
        return self._values[0]

    @z_min.setter
    def z_min(self, new):
        self._values[0] = new

    @property
    def z_max(self):
        return self._values[1]

    @z_max.setter
    def z_max(self, new):
        self._values[1] = new

    def _rvs(self, size=1):
        return np.exp(np.random.uniform(np.log(1+self.z_max), np.log(1+self.z_min), size=(size, 1))) - 1

    def _pdf(self, z):
        return (self.min_z<=z)*(z<self.max_z) / ((1+z)*(np.log(1+self.z_max)-np.log(1+self.z_min)))

    def _jacobian(self, z):
        pdf = self.pdf(z)
        return np.array((
                +pdf / ((np.log(1+self.z_max)-np.log(1+self.z_min))*(1+self.z_min))
                -pdf / ((np.log(1+self.z_max)-np.log(1+self.z_min))*(1+self.z_max))
            ),
            dtype=float,
        )

    def _hessian(self, z):
        ans = np.empty((2,2), dtype=float)
        ans[0,0] = 2./((1+z)*(np.log(1+self.z_max)-np.log(1+self.z_min))**3*(1+self.z_min)**2) - 1./((1+z)*(np.log(1+self.z_max)-np.log(1+self.z_min))**2*(1+self.z_min)**2)
        ans[0,1] = ans[1,0] = -2./((1+z)*(np.log(1+self.z_max)-np.log(1+self.z_min))**3*(1+self.z_max)*(1+self.z_min)),
        ans[1,1] = 2./((1+z)*(np.log(1+self.z_max)-np.log(1+self.z_min))**3*(1+self.z_max)**2) + 1./((1+z)*(np.log(1+self.z_max)-np.log(1+self.z_min))**2*(1+self.z_max)**2)
        return ans

class ParetoRedshift(RedshiftDistribution):
    """Obtain redshift distributed as (1+z)^index
    """
    _params = ('z_min', 'z_max', 'index')

    def __init__(self, z_min=0, z_max=10, index=3):
        assert z_min < z_max
        if index < 0:
            assert z_min > 0
        self._values = np.array([z_min, z_max, index], dtype=float)

    @property
    def z_min(self):
        return self._values[0]

    @z_min.setter
    def z_min(self, new):
        self._values[0] = new

    @property
    def z_max(self):
        return self._values[1]

    @z_max.setter
    def z_max(self, new):
        self._values[1] = new

    @property
    def index(self):
        return self._values[2]

    @index.setter
    def index(self, new):
        self._values[2] = new

    def _rvs(self, size=1):
        a, b, idx = self._values
        norm = (1 + b)**(idx+1) - (1 + a)**(idx+1)
        rvs = np.random.uniform(0, 1, size=(size, 1))
        return (norm * rvs + (1 + a)**(idx+1))**(1.0/(idx+1)) - 1
        # Despite its name, it can handle positive powers too.
        #return (negative_power(z_min, z_max, index),)

    def _pdf(self, z):
        return (self.index+1) * (1+z)**self.index / ((1 + self.z_max)**(self.index+1) - (1 + self.z_min)**(self.index+1))

    def _jacobian(self, z):
        m = 1 + self.z_min
        M = 1 + self.z_max
        n1 = self.index + 1
        z1 = 1 + z
        return np.array((
            +n1**2 * z1**self.index * m**self.index / (M**n1 - m**n1)**2, ### dp/dz_min
            -n1**2 * z1**self.index * M**self.index / (M**n1 - m**n1)**2, ### dp/dz_max
            z1**self.index / (M**n1 - m**n1) + n1*z1**self.index*np.log(z1)/(M**n1 - m**n1) - n1*z1**self.index/(M**n1 - m**n1)**2 * (M**n1 * np.log(M) - m**n1 * np.log(m)), ### dp/dindex
        ), dtype=float)        

    def _hessian(self, z):
        m = 1 + self.z_min
        M = 1 + self.z_max
        n1 = self.index + 1
        z1 = 1 + z

        ans = np.empty((3,3), dtype=float)
        ans[0,0] = n1**2*z1**self.index * self.index * m**(self.index-1) / (M**n1 - m**n1)**2 \
            + 2*n1**3 * z1**self.index * m**(2*self.index) / (M**n1 - m**n1)**3
        ans[0,1] = ans[1,0] = -2 * n1**3 * z1**self.index * M**self.index * m**self.index / (M**n1 - m**n1)**3
        ans[0,2] = ans[2,0] = + 2*n1 * z1**self.index * m**self.index / (M**n1 - m**n1)**2 \
            + n1**2 * z1**self.index * np.log(z1) * m**self.index / (M**n1 - m**n1)**2 \
            + n1**2 * z1**self.index * m**self.index * np.log(m) / (M**n1 - m**n1)**2 \
            + 2*n1**2 * z1**self.index * m**self.index * (M**n1 * np.log(M) - m**n1 * np.log(m))/ (M**n1 - m**n1)**3
        ans[1,1] = - n1**2*z1**self.index * self.index * M**(self.index-1) / (M**n1 - m**n1)**2 \
            + 2*n1**3 * z1**self.index * M**(2*self.index) / (M**n1 - m**n1)**3
        ans[1,2] = ans[2,1] = - 2*n1 * z1**self.index * M**self.index / (M**n1 - m**n1)**2 \
            - n1**2 * z1**self.index * np.log(z1) * M**self.index / (M**n1 - m**n1)**2 \
            - n1**2 * z1**self.index * M**self.index * np.log(M) / (M**n1 - m**n1)**2 \
            + 2*n1**2 * z1**self.index * M**self.index * (M**n1 * np.log(M) - m**n1 * np.log(m))/ (M**n1 - m**n1)**3
        ans[2,2] = z1**self.index * np.log(z1) / (M**n1 - m**n1) \
            - z1**self.index * (M**n1 * np.log(M) - m**n1 * np.log(m)) / (M**n1 - m**n1) \
            + z1**self.index * np.log(z1) / (M**n1 - m**n1) \
            + n1 * z1**self.index * np.log(z1)**2 / (M**n1 - m**n1) \
            - n1 * z1**self.index * np.log(z1) * (M**n1 * np.log(M) - m**n1 * np.log(m)) / (M**n1 - m**n1)**2 \
            - z1**self.index * (M**n1 * np.log(M) - m**n1 * np.log(m)) / (M**n1 - m**n1)**2 \
            - n1 * z1**self.index * np.log(z1) * (M**n1 * np.log(M) - m**n1 * np.log(m)) / (M**n1 - m**n1)**2 \
            - n1 * z1**self.index * (M**n1 * np.log(M)**2 - m**n1 * np.log(m)**2) / (M**n1 - m**n1)**2 \
            + 2 * n1 * z1**self.index * (M**n1 * np.log(M) - m**n1 * np.log(m))**2 / (M**n1 - m**n1)**3
        
        return ans

class UniformRedshift(RedshiftDistribution):
    """Uniform in redshift between zmin=0 (default) and zmax.
    """
    _params = ('z_min', 'z_max')

    def __init__(self, z_min=0, z_max=10):
        assert z_min < z_max
        self._values = np.array([z_min, z_max], dtype=float)

    @property
    def z_min(self):
        return self._values[0]

    @z_min.setter
    def z_min(self, new):
        self._values[0] = new

    @property
    def z_max(self):
        return self._values[1]

    @z_max.setter
    def z_max(self, new):
        self._values[1] = new

    def _rvs(self, size=1):
        return np.random.uniform(self.z_min, self.z_max, size=(size, 1))

    def _pdf(self, z):
        return (self.z_min <= z)*(z <= self.z_max)/(self.z_max-self.z_min)

    def _jacobian(self, z):
        ans = (self.z_min <= z)*(z <= self.z_max)/(self.z_max-self.z_min)**2
        return np.array((ans, -ans), dtype=float)

    def _hessian(self, z):
        ans = 2*(self.z_min <= z)*(z <= self.z_max)/(self.z_max-self.z_min)**3
        return np.ans(((ans, -ans), (-ans, ans)), dtype=float)

class UniformComovingRedshift(RedshiftDistribution):
    """Obtain redshift distributed as uniform in comoving volume.
    """
    _params = ('z_min', 'z_max')

    def __init__(self, z_min=0, z_max=10):
        assert z_min < z_max
        self._values = np.array([z_min, z_max], dtype=float)

    @property
    def z_min(self):
        return self._values[0]

    @z_min.setter
    def z_min(self, new):
        self._values[0] = new
        self._update()

    @property
    def z_max(self):
        return self._values[1]

    @z_max.setter
    def z_max(self, new):
        self._values[1] = new
        self._update()

    def _update(self):
        self._fmin = _com(self.z_min) if z_min > 0 else 0
        self._fmax = _com(self.z_max)
        assert self._fmin < self._fmax

    def _rvs(self, size=1):
        f = np.random.uniform(self._fmin, self._fmax, size=(size, 1))
        return _inv_com(f)

    def _pdf(self, z):
        """dVc/dz
        """
        raise NotImplementedError('''need access to dVc/dz from astropy.cosmo''')

    def _jacobian(self, z):
        """(dVc/dz|z)/(Vc(zmax) - Vc(zmin)**2 *(dVc/dz|zmin) , -(dVc/dz|z)/(Vc(zmax) - Vc(zmin))**2 *(dVc/dz|zmax)
        """
        raise NotImplementedError('''need access to dVc/dz from astropy.cosmo''')

    def _hessian(self, z):
        raise NotImplementedError('''need access to dVc/dz, d^2Vc/dz^2 from astropy.cosmo''')

class FishbachRedshift(RedshiftDistribution):
    """Redshift distribution ``dV_dz*(1+z)**(a-1)``.
    """
    _params = ('z_max', 'a')

    def __init__(self, z_max=2, a=3):
        self._values = np.array([z_max, a], dtype=float)
        self._update()

    @property
    def z_max(self):
        return self._values[0]

    @z_max.setter
    def z_max(self, new):
        self._values[0] = new
        self._update()

    @property
    def a(self):
        return self._values[1]

    @a.setter
    def a(self, new):
        self._values[1] = new
        self._update()

    def _update(self):
        from astropy.cosmology import Planck15
        import astropy.units as u

        zi = np.expm1(np.linspace(np.log(1), np.log(1+self.z_max), 1024))
        pi = Planck15.differential_comoving_volume(zi).to(u.Gpc**3/u.sr).value*(1+zi)**(self.a-1)
        ci = cumtrapz(pi, zi, initial=0)
        pi /= ci[-1]
        ci /= ci[-1]

        self._invcdf_interpolator = interp1d(ci, zi)
        self._pdf_interpolator = interp1d(zi, pi)

    def _rvs(self, size=1):
        return self._invcdf_interpolator(np.random.random(size=(size, 1)))

    def _pdf(self, z):
        return self._pdf_interpolator(z)

    def _jacobian(self, z):
        raise NotImplementedError("haven't computed Jacobian yet")

    def _hessian(self, z):
        return NotImplementedError("haven't computed Hessian yet")

### Distance generators
class UniformVolumeDistance(DistanceDistribution):
    """
    Uniform in Euclidean volume. E. g. p(r) \propto r^2
    """
    _params = ('d_max',)

    def __init__(self, d_max=1000):
        self._values = np.array([d_max], dtype=float)

    @property
    def d_max(self):
        return self._values[0]

    @d_max.setter
    def d_max(self, new):
        self._values[0] = new

    def _rvs(self, size=1):
        # Note power ~ (idx - 1)
        return np.random.power(3, size=(size, 1)) * self.d_max

    def _pdf(self, d):
        return 3 * d**2 / self.d_max**3

    def _jacobian(self, d):
        return np.array([-9*d**2 / self.d_max**4], dtype=float)

    def _hessian(self, d):
        return np.array([[36*d**2 / self.d_max**5]], dtype=float)

class UniformComovingDistance(DistanceDistribution):
    """uniform in comoving volume distribution, returning luminosity distance
    """
    _params = ()

    def _rvs(self, size=1):
        from astropy.cosmology import Planck15
        return Planck15.luminosity_distance(UniformComovingRedshift.rvs(self, size=(size, 1)))

    def _pdf(self, d):
        """(dVc/dz) /  (dDL/dz)
        """
        raise NotImplementedError('''need access to some stuff from astropy.cosmo''')

    def _jacobian(self, d):
        return np.array([], dtype=float)

    def _hessian(self, d):
        return np.array([[]], dtype=float)

#-------------------------------------------------
# Mass generators
#-------------------------------------------------

class FixedComponentMass(MassDistribution):
    """Generate mass pairs with specific values.
    """
    _params = ('mass1', 'mass2')

    def __init__(self, mass1=10., mass2=10.):
        self._values = np.array([mass1, mass2], dtype=float)

    @property
    def mass1(self):
        return self._values[0]

    @mass1.setter
    def mass1(self, new):
        self._values[0] = new

    @property
    def mass2(self):
        return self._values[1]

    @mass2.setter
    def mass2(self, new):
        self._values[1] = new

    def _rvs(self, size=1):
        ans = np.empty((size, 2), dtype=float)
        ans[:,0] = self.mass1
        ans[:,1] = self.mass2
        return ans

    def _pdf(self, mass1, mass2):
        if mass1==self.mass1 and mass2==self.mass2:
            return 1.
        else:
            return 0.

    def _jacobian(self, mass1, mass2):
        return np.zeros(2, type=float)

    def _hessian(self, mass1, mass2):
        return np.zeros((2,2), dtype=float)

class UniformComponentMass(MassDistribution):
    _params = ('min_primary', 'max_primary', 'min_secondary', 'max_secondary')

    def __init__(self, min_primary=1.0, max_primary=100.0, min_secondary=1.0, max_secondary=100.0):
        self._values = np.array([min_primary, max_primary, min_secondary, max_secondary], dtype=float)

    @property
    def min_primary(self):
        return self._values[0]

    @min_primary.setter
    def min_primary(self, new):
        self._values[0] = new

    @property
    def max_primary(self):
        return self._values[1]

    @max_primary.setter
    def max_primary(self, new):
        self._values[1] = new

    @property
    def min_secondary(self):
        return self._values[2]

    @min_secondary.setter
    def min_secondary(self, new):
        self._values[2] = new

    @property
    def max_secondary(self):
        return self._values[3]

    @max_secondary.setter
    def max_secondaryy(self, new):
        self._values[3] = new

    def _rvs(self, size=1):
        return np.array(
            zip(
                np.random.uniform(self.min_primary, self.max_primary, size=size),
                np.random.uniform(self.min_secondary, self.max_secondary, size=size)
            ),
            dtype=float
        )

    def _pdf(self, mass1, mass2):
        ans = 1.
        ans *= (self.min_primary <= mass1)*(mass1 < self.max_primary) / (self.max_primary - self.min_primary) 
        ans *= (self.min_secondary <= mass2)*(mass2 < self.max_secondary) / (self.max_secondary - self.max_secondary)
        return ans

    def _jacobian(self, mass1, mass2):
        base = (self.min_primary <= mass1)*(mass1 < self.max_primary)*(self.min_secondary <= mass2)*(mass2 < self.max_secondary)
        ans = base / ((self.max_primary - self.min_primary)**2 * (self.max_secondary - self.max_secondary))
        wer = base / ((self.max_primary - self.min_primary) * (self.max_secondary - self.max_secondary)**2)
        return np.array((ans, -ans, wer, -wer), dtype=float)

    def _hessian(self, mass1, mass2):
        base = (self.min_primary <= mass1)*(mass1 < self.max_primary)*(self.min_secondary <= mass2)*(mass2 < self.max_secondary)
        ans = np.empty((4,4), dtype=float)

        ans[0,0] = 2 * base / ((self.max_primary - self.min_primary)**3 * (self.max_secondary - self.max_secondary))
        ans[0,1] = ans[1,0] = - 2 * base / ((self.max_primary - self.min_primary)**3 * (self.max_secondary - self.max_secondary))
        ans[0,2] = ans[2,0] = +base / ((self.max_primary - self.min_primary)**2 * (self.max_secondary - self.max_secondary)**2)
        ans[0,3] = ans[3,0] = -base / ((self.max_primary - self.min_primary)**2 * (self.max_secondary - self.max_secondary)**2)
        ans[1,1] = 2 * base / ((self.max_primary - self.min_primary)**3 * (self.max_secondary - self.max_secondary))
        ans[1,2] = ans[2,1] = -base / ((self.max_primary - self.min_primary)**2 * (self.max_secondary - self.max_secondary)**2)
        ans[1,3] = ans[3,1] = +base / ((self.max_primary - self.min_primary)**2 * (self.max_secondary - self.max_secondary)**2)
        ans[2,2] = 2 * base / ((self.max_primary - self.min_primary) * (self.max_secondary - self.min_secondary)**3)
        ans[2,3] = ans[3,2] = -2 * base / ((self.max_primary - self.min_primary) * (self.max_secondary - self.max_secondary)**3)
        ans[3,3] = 2 * base / ((self.max_primary - self.min_primary) * (self.max_secondary - self.min_secondary)**3)

        return ans

class NormalComponentMass(MassDistribution):
    _params = ('mean_primary', 'std_primary', 'mean_secondary', 'std_secondary')

    def __init__(self, mean_primary=1.4, std_primary=0.1, mean_secondary=1.4, std_secondary=0.1):
        self._values = np.array([mean_primary, std_primary, mean_secondary, std_secondary], dtype=float)

    @property
    def mean_primary(self):
        return self._values[0]

    @mean_primary.setter
    def mean_primary(self, new):
        self._values[0] = new

    @property
    def std_primary(self):
        return self._values[1]

    @std_primary.setter
    def std_primary(self, new):
        self._values[1] = new

    @property
    def mean_secondary(self):
        return self._values[2]

    @mean_secondary.setter
    def mean_secondary(self, new):
        self._values[2] = new

    @property
    def std_secondary(self):
        return self._values[3]

    @std_secondary.setter
    def std_secondary(self, new):
        self._values[3] = new

    def _rvs(self, size=1):
        return np.array(
            zip(
                np.random.normal(self.mean_primary, self.std_primary, size=size),
                np.random.normal(self.mean_secondary, self.std_secondary, size=size)
            ),
            dtype=float
        )

    def _pdf(self, mass1, mass2):
        return np.exp(-0.5*(mass1-self.mean_primary)**2/self.std_primary**2 -0.5*(mass2-self.mean_secondary)**2/self.std_secondary**2) / (2*np.pi * self.std_primary * self.std_secondary)

    def _jacobian(self, mass1, mass2):
        pdf = self.pdf(mass1, mass2)
        return np.array((
                pdf * (mass1-self.mean_primary)/self.std_primary**2,
                pdf * (-1/self.std_primary + (mass1-self.mean_primary)**2/self.std_primary**3),
                pdf * (mass2-self.mean_secondary)/self.std_secondary**2,
                pdf * (-1/self.std_secondary + (mass1-self.mean_secondary)**2/self.std_secondary**3),
            ),
            dtype=float
        )

    def _hessian(self, mass1, mass2):
        ans = np.empty((4,4), dtype=float)
        pdf = self.pdf(mass1, mass2)

        ans[0,0] = pdf * (mass1-self.mean_primary)**2/self.std_primary**2 - pdf/self.std_primary**2
        ans[0,1] = ans[1,0] = -3 * pdf * (mass1-self.mean_primary)/self.std_primary**3 \
            + pdf * (mass1-self.mean_primary)**3/self.std_primary**5
        ans[0,2] = ans[2,0] = pdf * (mass1-self.mean_primary)/self.std_primary**2 * (mass2-self.mean_secondary)/self.std_secondary**2
        ans[0,3] = ans[3,0] = -pdf/self.std_secondary**2 \
            + pdf * (mass2-self.mean_secondary)**2/self.std_secondary**3 * (mass1-self.mean_primary)/self.std_primary**2
        ans[1,1] = pdf * 2/self.std_primary**2 \
            - 2 * pdf * (mass1-self.mean_primary)**2/self.std_primary**4 \
            + pdf * (mass1-self.mean_primary)**4/self.std_primary**6 \
            - 3 * pdf * (mass1-self.mean_primary)**2/self.std_primary**4
        ans[1,2] = ans[2,1] = -pdf/self.std_primary**2 \
            + pdf * (mass1-self.mean_primary)**2/self.std_primary**3 * (mass2-self.mean_secondary)/self.std_secondary**2
        ans[1,3] = ans[3,1] = pdf/(self.std_primary*self.std_secondary) \
            - pdf * (mass1-self.mean_primary)/(self.std_secondary*self.std_primary**3) \
            - pdf * (mass2-self.mean_secondary)/(self.std_primary*self.std_secondary**3) \
            + pdf * (mass1-self.mean_primary)**2/self.std_primary**3 * (mass2-self.mean_secondary)**2/self.std_secondary**3
        ans[2,2] = pdf * (mass2-self.mean_secondary)**2/self.std_secondary**2 - pdf/self.std_secondary**2
        ans[2,3] = ans[3,2] = -3 * pdf * (mass2-self.mean_secondary)/self.std_secondary**3 \
            + pdf * (mass2-self.mean_secondary)**3/self.std_secondary**5
        ans[4,4] = pdf * 2/self.std_secondary**2 \
            - 2 * pdf * (mass2-self.mean_secondary)**2/self.std_secondary**4 \
            + pdf * (mass2-self.mean_secondary)**4/self.std_secondary**6 \
            - 3 * pdf * (mass2-self.mean_secondary)**2/self.std_secondary**4

        return ans

class AstroComponentMass(MassDistribution):
    _params = ('min_primary', 'max_primary', 'mean_secondary', 'std_secondary', 'power_idx')

    def __init__(self, min_primary=5.0, max_primary=100.0, mean_secondary=1.4, std_secondary=0.1, power_idx=-2.3):
        self._values = np.array([min_primary, max_primary, mean_secondary, std_secondary, power_idx], dtype=float)

    @property
    def min_primary(self):
        return self._values[0]

    @min_primary.setter
    def min_primary(self, new):
        self._values[0] = new

    @property
    def max_primary(self):
        return self._values[1]

    @max_primary.setter
    def max_primary(self, new):
        self._values[1] = new

    @property
    def mean_secondary(self):
        return self._values[2]

    @mean_secondary.setter
    def mean_secondary(self, new):
        self._values[2] = new

    @property
    def std_secondary(self):
        return self._values[3]

    @std_secondary.setter
    def std_secondary(self, new):
        self._values[3] = new

    @property
    def power_idx(self):
        return self._values[4]

    @power_idx.setter
    def power_idx(self, new):
        self._values[4] = new

    def _rvs(self, size=1):
        return np.array(
            zip(
                negative_power(self.min_primary, self.max_primary, self.power_idx, size=size),
                np.random.normal(self.mean_secondary, self.std_secondary, size=size)
            ),
            dtype=float
        )

    def _pdf(self, mass1, mass2):
        ans = 1.
        ans *= (self.min_primary <= mass1)*(mass1 < self.max_primary) * (self.power_idx+1) * mass1**self.power_idx / (self.max_primary**(self.power_idx+1) - self.min_primary**(self.power_idx+1))
        ans *= np.exp(-0.5*(mass2-self.mean_secondary)**2/self.std_secondary**2) / ((2*np.pi)**0.5*self.std_secondary)
        return ans

    def _jacobian(self, mass1, mass2):
        pdf = self.pdf(mass1, mass2)
        return np.array((
            +pdf * (self.power_idx+1)*self.min_primary**self.power_idx / (self.max_primary**(self.power_idx+1)-self.min_primary**(self.power_idx+1)),
            -pdf * (self.power_idx+1)*self.max_primary**self.power_idx / (self.max_primary**(self.power_idx+1)-self.min_primary**(self.power_idx+1)),
            +pdf * (mass2-self.mean_secondary)/self.std_secondary**2,
            -pdf / self.std_secondary + pdf * (mass2-self.mean_secondary)**2 / self.std_secondary**3,
            +pdf / (self.power_idx+1) + pdf * np.log(mass1) - pdf * (self.max_primary**(self.power_idx+1)*np.log(self.max_primary) - self.min_primary**(self.power_idx+1)*np.log(self.min_primary)) / (self.max_primary**(self.power_idx+1) - self.min_primary**(self.power_idx+1)),
            ),
            dtype=float,
        )

    def _hessian(self, mass1, mass2):
        raise NotImplementedError

class ParetoFlatComponentMass(MassDistribution):
    """Generate BBH masses. Primary is a power law (-2.3) in 5 - 50 and the secondary is generated according to a uniform distribution in mass ratio such that min_q < q < m_max_q.
    """
    _params = ('min_bh_mass', 'max_bh_mass', 'min_q', 'max_q', 'power_idx')

    def __init__(self, min_bh_mass=10, max_bh_mass=100., min_q=0.1, max_q=1.0, power_idx=-2.3):
        assert 0 < min_q
        assert min_q <= max_q
        assert max_q <= 1
        self._values = np.array([min_bh_mass, max_bh_mass, min_q, max_q, power_idx], dtype=float)

    @property
    def min_bh_mass(self):
        return self._values[0]

    @min_bh_mass.setter
    def min_bh_mass(self, new):
        self._values[0] = new

    @property
    def max_bh_mass(self):
        return self._values[1]

    @max_bh_mass.setter
    def max_bh_mass(self, new):
        self._values[1] = new

    @property
    def min_q(self):
        return self._values[2]

    @min_q.setter
    def min_q(self, new):
        self._values[2] = new

    @property
    def max_q(self):
        return self._values[3]

    @max_q.setter
    def max_q(self, new):
        self._values[3] = new

    @property
    def power_idx(self):
        return self._values[4]

    @power_idx.setter
    def power_idx(self, new):
        self._values[4] = new

    def _rvs(self, size=1):
        m1 = negative_power(self.min_bh_mass, self.max_bh_mass, self.power_idx, size=size)
        return np.array(
            zip(
                m1,
                np.random.uniform(self.min_q, self.max_q, size=size) * m1
            ),
            dtype=float
        )

    def _pdf(self, mass1, mass2):
        q = float(mass2)/mass1
        ans = (self.min_bh_mass <= mass1)*(mass1 < self.max_bh_mass)*(self.min_q<=q)*(q<self.max_q)
        ans *= mass1**self.power_idx ### pareto distrib for mass1
        ans *= (self.power_idx+1)/(self.max_bh_mass**(self.power_idx+1) - self.min_bh_mass**(self.power_idx+1))
        ans *= 1./mass1 ### jacobian between flat-in-q and mass2
        ans *= 1./(self.max_q - self.min_q) ### prior normalization from q
        return ans

    def _jacobian(self, mass1, mass2):
        pdf = self.pdf(mass1, mass2)
        return np.array((
                +pdf * (self.power_idx+1)*self.min_bh_mass**self.power_idx / (self.max_bh_mass**(self.power_idx+1)-self.min_bh_mass**(self.power_idx+1)),
                -pdf * (self.power_idx+1)*self.max_bh_mass**self.power_idx / (self.max_bh_mass**(self.power_idx+1)-self.min_bh_mass**(self.power_idx+1)),
                +pdf / (self.max_q - self.min_q),
                -pdf / (self.max_q - self.min_q),
                +pdf / (self.power_idx+1) + pdf * np.log(mass1) - pdf * (self.max_bh_mass**(self.power_idx+1) * np.log(self.max_bh_mass) - self.min_bh_mass**(self.power_idx)*np.log(self.min_bh_mass)) / (self.max_bh_mass**(self.power_idx+1)-self.min_bh_mass**(self.power_idx+1))
            ),
            dtype=float,
        )

    def _hessian(self, mass1, mass2):
        raise NotImplementedError

#-------------------------------------------------
# Mass Generators conditioned on Redshift
#-------------------------------------------------

class ParetoFlatComponentMassConditionedOnRedshift(ParetoFlatComponentMass):
    """a "smart" distribution that combines the ParetoFlatComponentMass distribution with the FishbachRedshift distribution in an attempt to sample more efficiently
    """
    _required = ('z',)

    _max_redshift = 2.0 ### the maximum redshift for which we expect our approximation in z2min_bh_mass to hold

    def __init__(self, min_bh_mass=10, max_bh_mass=100., min_q=0.1, max_q=1.0, power_idx=-2.3):
        ParetoFlatComponentMass.__init__(self, min_bh_mass=min_bh_mass, max_bh_mass=max_bh_mass, min_q=min_q, max_q=max_q, power_idx=power_idx)
        self._static_min_bh_mass = min_bh_mass ### hold this outside of the parent object's structures so we can swap it back and forth
        self._update()

    def z2min_bh_mass(self, z):
        """a helper function that figures out the minimum BH mass from a redshift

        **NOTE**, this is a naive (hopefully conservative) estimate based on a larger monte-carlo study
        """
        assert z <= self._max_redshift, 'our approximate scaling relation for the minimum BH mass as a function of z is only valid up to z=%.3d'%self._max_redshift
        return min(self.max_bh_mass, max(self._static_min_bh_mass, 50 * z)) ### this relation should hopefully be valid up to z~1.5

    def _juggle_min_bh_mass_(self, foo, *args, **kwargs):
        args, z = args[:-1], args[-1] ### this should always be true
        try:
            self.min_bh_mass = self.z2min_bh_mass(z)
            return foo(self, *args, **kwargs) ### call the parent class's method
        except: ### do this so we always re-set the component mass
            raise
        finally:
            self.min_bh_mass = self._static_min_bh_mass ### re-set this to the original value

    def _rvs(self, size=1, *args):
        """args is specified like this for syntactic reasons, but it should just be the redshift
        """
        z = args[0]
        return self._juggle_min_bh_mass_(ParetoFlatComponentMass._rvs, size, z)

    def _pdf(self, mass1, mass2, z):
        return self._juggle_min_bh_mass_(ParetoFlatComponentMass._pdf, mass1, mass2, z)

    def _jacobian(self, mass1, mass2, z):
        return self._juggle_min_bh_mass_(ParetoFlatComponentMass._jacobian, mass1, mass2, z)

    def _hessian(self, mass1, mass2, z):
        return self._juggle_min_bh_mass_(ParetoFlatComponentMass._hessian, mass1, mass2, z)

#-------------------------------------------------
# Spin generators
#-------------------------------------------------
class ZeroSpin(SpinDistribution):
    _params = ()

    def _rvs(self, size=1):
        return np.zeros((size, 6), dtype=float)

    def _pdf(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        if (spin1x==0) and (spin1y==0) and (spin1z==0) and (spin2x==0) and (spin2y==0) and (spin2z==0):
            return 1.
        else:
            return 0.

    def _jacobian(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        return np.array([], dtype=float)

    def _hessian(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        return np.array([[]], dtype=float)

class UniformSpin(SpinDistribution):
    """Generate isotropically distributed spins. The spin components magnitudes are uniform.
    """
    _params = ('a1_max', 'a2_max', 'a1_min', 'a2_min')

    def __init__(self, a1_max=1.0, a2_max=1.0, a1_min=0.0, a2_min=0.0):
        self._values = np.array([a1_max, a2_max, a1_min, a2_min], dtype=float)

    @property
    def a1_max(self):
        return self._values[0]

    @a1_max.setter
    def a1_max(self, new):
        self._values[0] = new

    @property
    def a2_max(self):
        return self._values[1]

    @a2_max.setter
    def a2_max(self, new):
        self._values[1] = new

    @property
    def a1_min(self):
        return self._values[2]

    @a1_min.setter
    def a1_min(self):
        self._values[2] = new

    @property
    def a2_min(self):
        return self._values[3]

    @a2_min.setter
    def a2_min(self, new):
        self._values[3] = new

    def _rvs(self, size=1):
        a1 = np.random.uniform(self.a1_min, self.a1_max, size=size)
        a2 = np.random.uniform(self.a2_min, self.a2_max, size=size)

        th1 = np.arcsin(np.random.uniform(-1, 1, size=size))
        th2 = np.arcsin(np.random.uniform(-1, 1, size=size))

        ph1 = np.random.uniform(0, 2 * np.pi, size=size)
        ph2 = np.random.uniform(0, 2 * np.pi, size=size)

        s1x = a1 * np.cos(th1) * np.cos(ph1)
        s1y = a1 * np.cos(th1) * np.sin(ph1)
        s1z = a1 * np.sin(th1)

        s2x = a2 * np.cos(th2) * np.cos(ph2)
        s2y = a2 * np.cos(th2) * np.sin(ph2)
        s2z = a2 * np.sin(th2)

        return np.array(zip(s1x, s1y, s1z, s2x, s2y, s2z), dtype=float)

    def _pdf(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        a1 = (spin1x**2 + spin1y**2 + spin1z**2)**0.5
        a2 = (spin2x**2 + spin2y**2 + spin2z**2)**0.5
        return (self.a1_min <= a1)*(a1 < self.a1_max)*(self.a2_min <= a2)*(a2 < self.a2_max) / ((self.a1_max-self.a1_min)*(self.a2_max-self.a2_min))

    def _jacobian(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        pdf = self.pdf(spin1x, spin1y, spin1z, spin2x, spin2y, spin2z)
        return np.array((
                -pdf/(self.a1_max-self.a1_min),
                -pdf/(self.a2_max-self.a2_min),
                +pdf/(self.a1_max-self.a1_min),
                +pdf/(self.a2_max-self.a2_min),
            ),
            dtype=float,
        )

    def _hessian(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        pdf = self.pdf(spin1x, spin1y, spin1z, spin2x, spin2y, spin2z)

        ans = np.empty((4,4), dtype=float)

        ans[0,0] = 2*pdf/(self.a1_max-self.a1_min)**2
        ans[0,1] = ans[1,0] = pdf / ((self.a1_max-self.a1_min)*(self.a2_max-self.a2_min))
        ans[0,2] = ans[2,0] = -2*pdf / (self.a1_max-self.a1_min)**2
        ans[0,3] = ans[3,0] = -pdf / ((self.a1_max-self.a1_min)*(self.a2_max-self.a2_min))
        ans[1,1] = 2*pdf/(self.a2_max-self.a2_min)**2
        ans[1,2] = ans[2,1] = -pdf / ((self.a1_max-self.a1_min)*(self.a2_max-self.a2_min))
        ans[1,3] = ans[3,1] = -2*pdf / (self.a2_max-self.a2_min)**2
        ans[2,2] = 2*pdf/(self.a1_max-self.a1_min)**2
        ans[2,3] = ans[3,2] = pdf / ((self.a1_max-self.a1_min)*(self.a2_max-self.a2_min))
        ans[3,3] = 2*pdf/(self.a2_max-self.a2_min)**2

        return ans

class UniformAlignedSpin(SpinDistribution):
    """Generate spins aligned with the orbital angular momentum. The spin magnitude is distributed uniformly up to maximum.
    """
    _params = ('a1_max', 'a2_max')

    def __init__(self, a1_max=1.0, a2_max=1.0):
        self._values = np.array([a1_max, a2_max], dtype=float)

    @property
    def a1_max(self):
        return self._values[0]

    @a1_max.setter
    def a1_max(self, new):
        self._values[0] = new

    @property
    def a2_max(self):
        return self._values[1]

    @a2_max.setter
    def a2_max(self, new):
        self._values[1] = new

    def _rvs(self, size=1):
        ans = np.zeros((size, 6), dtype=float)
        ans[:,2] = np.random.uniform(-self.a1_max, self.a1_max, size=size)
        ans[:,5] = np.random.uniform(-self.a2_max, self.a2_max, size=size)
        return ans

    def _pdf(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        if (spin1x==0) and (spin1y==0) and (np.abs(spin1z)<self.a1_max) and (spin2x==0) and (spin2y==0) and (np.abs(spin2z)<self.a2_max):
            return 0.25/(self.a1_max*self.a2_max)
        else:
            return 0.

    def _jacobian(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        pdf = self.pdf(spin1x, spin1y, spin1z, spin2x, spin2y, spin2z)
        return np.array((
                -pdf/self.a1_max,
                -pdf/self.a2_max,
            ),
            dtype=float,
        )

    def _hessian(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        pdf = self.pdf(spin1x, spin1y, spin1z, spin2x, spin2y, spin2z)
        ans = np.array((2,2), dtype=float)
        ans[0,0] = 2*pdf/self.a1_max**2
        ans[0,1] = ans[1,0] = pdf/(self.a1_max*self.a2_max)
        ans[1,1] = 2*pdf/self.a2_max**2
        return ans

class IsotropicSpin(SpinDistribution):
    """Generate isotropically distributed spins. The spin components magnitudes are isotropic. E.g. this will favor large spin magnitudes.
    """
    _params = ('a1_max', 'a2_max', 'a1_min', 'a2_min')

    def __init__(self, a1_max=1.0, a2_max=1.0, a1_min=0.0, a2_min=0.0):
        self._values = np.array([a1_max, a2_max, a1_min, a2_min], dtype=float)

    @property
    def a1_max(self):
        return self._values[0]

    @a1_max.setter
    def a1_max(self, new):
        self._values[0] = new

    @property
    def a2_max(self):
        return self._values[1]

    @a2_max.setter
    def a2_max(self, new):
        self._values[1] = new

    @property
    def a1_min(self):
        return self._values[2]

    @a1_min.setter
    def a1_min(self, new):
        self._values[2] = new

    @property
    def a2_min(self):
        return self._values[3]

    @a2_min.setter
    def a2_min(self, new):
        self._values[3] = new

    def _rvs(self, size=1):
        a1 = ((self.a1_max**3 - self.a1_min**3) * np.random.random(size=size) + self.a1_min**3)**(1./3)
        a2 = ((self.a2_max**3 - self.a2_min**3) * np.random.random(size=size) + self.a1_min**3)**(1./3)

        th1 = np.arcsin(np.random.uniform(-1, 1, size=size))
        th2 = np.arcsin(np.random.uniform(-1, 1, size=size))

        ph1 = np.random.uniform(0, 2 * np.pi, size=size)
        ph2 = np.random.uniform(0, 2 * np.pi, size=size)

        s1x = a1 * np.cos(th1) * np.cos(ph1)
        s1y = a1 * np.cos(th1) * np.sin(ph1)
        s1z = a1 * np.sin(th1)

        s2x = a2 * np.cos(th2) * np.cos(ph2)
        s2y = a2 * np.cos(th2) * np.sin(ph2)
        s2z = a2 * np.sin(th2)

        return np.array(zip(s1x, s1y, s1z, s2x, s2y, s2z), dtype=float)

    def _pdf(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        a1 = (spin1x**2 + spin1y**2 + spin1z**2)**0.5
        a2 = (spin2x**2 + spin2y**2 + spin2z**2)**0.5
        return (self.a1_min <= a1)*(a1 < self.a1_max)*(self.a2_min <= a2)*(a2 < self.a2_max) * 9 * a1**2 * a2**2 / ((self.a1_max**3 - self.a1_min**3)*(self.a2_max**3 - self.a2_min**3))

    def _jacobian(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        pdf = self.pdf(spin1x, spin1y, spin1z, spin2x, spin2y, spin2z)
        return np.array((
                -pdf * 3*self.a1_max**2 / (self.a1_max**3 - self.a1_min**3),
                -pdf * 3*self.a2_max**2 / (self.a2_max**3 - self.a2_min**3),
                -pdf * 3*self.a1_min**2 / (self.a1_max**3 - self.a1_min**3),
                +pdf * 3*self.a2_min**2 / (self.a2_max**3 - self.a2_min**3),
            ),
            dtype=float,
        )

    def _hessian(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        pdf = self.pdf(spin1x, spin1y, spin1z, spin2x, spin2y, spin2z)
        ans = np.empty((4,4), dtype=float)

        ans[0,0] = -6*self.a1_max*pdf/(self.a1_max**3 - self.a1_min**3) + 6*self.a1_max**4*pdf/(self.a1_max**3 - self.a1_min**3)**2,
        ans[0,1] = ans[1,0] = +9*self.a1_max**2*self.a2_max**2*pdf/((self.a1_max**3 - self.a1_min**3)*(self.a2_max**3 - self.a2_min**3)),
        ans[0,2] = ans[2,0] = 18*self.a1_max**2*self.a1_min**2*pdf/(self.a1_max**3 - self.a1_min**3)**2,
        ans[0,3] = ans[3,0] = -9*self.a1_max**2*self.a2_min**2*pdf/((self.a1_max**3 - self.a1_min**3)*(self.a2_max**3 - self.a2_min**3)),
        ans[1,1] = -6*self._a2_max*pdf/(self.a2_max**3 - self.a2_min**3) + 6*self.a2_max**4*pdf/(self.a2_max**3 - self.a2_min**3)**2,
        ans[1,2] = ans[2,1] = -9*self.a1_min**2*self.a2_max**2*pdf/((self.a1_max**3 - self.a1_min**3)*(self.a2_max**3 - self.a2_min**3)),
        ans[1,3] = ans[3,1] = 18*self.a2_max**2*self.a2_min**2*pdf/(self.a2_max**3 - self.a2_min**3)**2,
        ans[2,2] = +6*self.a1_min*pdf/(self.a1_max**3 - self.a1_min**3) + 6*self.a1_min**4*pdf/(self.a1_max**3 - self.a1_min**3)**2,
        ans[2,3] = ans[3,2] = +9*self.a1_min**2*self.a2_min**2*pdf/((self.a1_max**3 - self.a1_min**3)*(self.a2_max**3 - self.a2_min**3)),
        ans[3,3] = +6*self._a2_min*pdf/(self.a2_max**3 - self.a2_min**3) + 6*self.a2_min**4*pdf/(self.a2_max**3 - self.a2_min**3)**2,

        return ans

class IsotropicAlignedSpin(SpinDistribution):
    """Generate spins aligned with the orbital angular momentum. The spin magnitude is distributed as if the spin vectors were isotropic but the in-plane spin component is ignored.
    """
    _params = ('a1_max', 'a2_max', 'a1_min', 'a2_min')

    def __init__(self, a1_max=1.0, a2_max=1.0, a1_min=0., a2_min=0.):
        self._values = np.array([a1_max, a2_max, a1_min, a2_min], dtype=float)

    @property
    def a1_max(self):
        return self._values[0]

    @a1_max.setter
    def a1_max(self, new):
        self._values[0] = new

    @property
    def a2_max(self):
        return self._values[1]

    @a2_max.setter
    def a2_max(self, new):
        self._values[1] = new

    @property
    def a1_min(self):
        return self._values[2]

    @a1_min.setter
    def a1_min(self, new):
        self._values[2] = new

    @property
    def a2_min(self):
        return self._values[3]

    @a2_min.setter
    def a2_min(self, new):
        self._values[3] = new

    def _rvs(self, size=1):
        ans = np.zeros((size, 6), dtype=float)
        a1 = ((self.a1_max**3 - self.a1_min**3) * np.random.random(size=size) + self.a1_min**3)**(1./3)
        a2 = ((self.a2_max**3 - self.a2_min**3) * np.random.random(size=size) + self.a1_min**3)**(1./3)
        ans[:,2] = a1 * np.arcsin(np.random.uniform(-1, 1, size=size))
        ans[:,5] = a2 * np.arcsin(np.random.uniform(-1, 1, size=size))
        return ans

    def _pdf(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        a1 = np.abs(spin1z)
        a2 = np.abs(spin2z)
        if (spin1x==0) and (spin1y==0) and (a1<self.a1_max) and (spin2x==0) and (spin2y==0) and (a2<self.a2_max):
            return 0.75**2 * (self.a1_max**2 - np.max(a1, self.a1_min)**2) * (self.a2_max**2 - np.max(a2, self.a2_min)**2) / ((self.a1_max**3 - self.a1_min**3)*(self.a2_max**3-self.a2_min**3))
        else:
            return 0.

    def _jacobian(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        a1 = np.abs(spin1z)
        a2 = np.abs(spin2z)
        return np.array((
                0.75**2*(self.a2_max**2 - np.max(a2, self.a2_min)**2)/(self.a2_max**3-self.a2_min**3)*( 2*self.a1_max/(self.a1_max**3 - self.a1_min**3) - (self.a1_max**2 - np.max(a1, self.a1_min)**2)*3*self.a1_max**2/(self.a1_max**3 - self.a1_min**3)**2 ),
                0.75**2*(self.a1_max**2 - np.max(a1, self.a1_min)**2)/(self.a1_max**3-self.a1_min**3)*( 2*self.a2_max/(self.a2_max**3 - self.a2_min**3) - (self.a2_max**2 - np.max(a2, self.a2_min)**2)*3*self.a2_max**2/(self.a2_max**3 - self.a2_min**3)**2 ),
                0.75**2*(self.a2_max**2 - np.max(a2, self.a2_min)**2)/(self.a2_max**3-self.a2_min**3)*( (self.a1_max**2 - np.max(a1, self.a1_min)**2)*3*self.a1_min**2/(self.a1_max**3 - self.a1_min**3)**2 - (self.a1_min > a1)*2*self.a1_min/(self.a1_max**3 - self.a1_min**3) ),
                0.75**2*(self.a1_max**2 - np.max(a1, self.a1_min)**2)/(self.a1_max**3-self.a1_min**3)*( (self.a2_max**2 - np.max(a2, self.a2_min)**2)*3*self.a2_min**2/(self.a2_max**3 - self.a2_min**3)**2 - (self.a2_min > a2)*2*self.a2_min/(self.a2_max**3 - self.a2_min**3) ),
            ),
            dtype=float,
        )

    def _hessian(self, spin1x, spin1y, spin1z, spin2x, spin2y, spin2z):
        raise NotImplementedError

#---------------------------------------------------------------------------------------------------
# Reference instantiations
#---------------------------------------------------------------------------------------------------

# Astrophysical object binary generators
uniform_bns_comp_mass = UniformComponentMass(min_primary=1.0, max_primary=2.0, min_secondary=1.0, max_secondary=2.0)
uniform_bbh_comp_mass = UniformComponentMass(min_primary=3.0, max_primary=100.0, min_secondary=3.0, max_secondary=100.0)
uniform_nsbh_comp_mass = UniformComponentMass(min_primary=3.0, max_primary=50.0, min_secondary=1.0, max_secondary=2.0)

# From Ozel and Freire (2016): https://arxiv.org/pdf/1603.02698.pdf
normal_bns_comp_mass = NormalComponentMass(mean_primary=1.33, std_primary=0.09, mean_secondary=1.33, std_secondary=0.09)
astro_nsbh_comp_mass = AstroComponentMass(min_primary=3.0, max_primary=50.0, mean_secondary=1.33, std_secondary=0.09, power_idx=-2.3)

# A few representative spin distributions
# NSBH, NS non-spinning
astro_spin = UniformSpin(a2_max=0.0)
