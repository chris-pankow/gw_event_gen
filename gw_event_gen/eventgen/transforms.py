"""a module that houses various population models useful for generating synthetic GW events and general population inference
"""

from __future__ import print_function

__author__ = "Chris Pankow <'chris.pankow@ligo.org'>, Reed Essick <reed.essick@gmail.com>"

#-------------------------------------------------

import numpy as np

from scipy.stats import ncx2

import lal
from lal.lal import GreenwichMeanSiderealTime as GMST

import lalsimulation

from .event import Event

#---------------------------------------------------------------------------------------------------
# Post filtering
#---------------------------------------------------------------------------------------------------

class AttributeTransformation(object):
    """a simple class that transforms Event attributes and add them to the object in place
    """
    _required = ()
    _variates = ()

    def check(self, event):
        for attr in self._required:
            assert hasattr(event, attr), 'Event does not have attribute "%s"'%attr

    def __call__(self, event):
        self.check(event)
        self.transform(event)

    @staticmethod
    def transform(event):
        pass

class Celestial2Geographic(AttributeTransformation):
    """computes the Geographic coordinates of the event from the Celestial coordinates and the time
    """
    _required = ('right_ascension', 'declination', 'time')
    _variates = ('theta', 'phi')

    @staticmethod
    def transform(event):
        setattr(event, 'phi', (event.right_ascension - GMST(event.time))%(2*np.pi))
        setattr(event, 'theta', 0.5*np.pi - event.declination)

class Redshift2ComovingDistance(AttributeTransformation):
    """compute comoving distance based on a redshift
    """
    _required = ('z',)
    _variates = ('comoving_distance',)

    @staticmethod
    def transform(event):
        from astropy.cosmology import Planck15
        setattr(event, 'comoving_distance', Planck15.comoving_distance(event.z).value)

class Redshift2LuminosityDistance(AttributeTransformation):
    """compute luminosity distance based on a redshift
    """
    _required = ('z',)
    _variates = ('distance',)

    @staticmethod
    def transform(event):
        from astropy.cosmology import Planck15
        setattr(event, 'distance', Planck15.luminosity_distance(event.z).value)

class LuminosityDistance2Redshift(AttributeTransformation):
    """compute redshift based on luminosity distance
    **WARNING**: this is implemented in a way that is known to be computationally expensive
    """
    _required = ('distance',)
    _variates = ('z',)

    @staticmethod
    def transform(event, rtol=1e-3):
        from astropy.cosmology import Planck15

        ### perform a bisection search
        zmin = 0
        dmin = 0
        zmax = 1.0
        dmax = Planck15.luminosity_distance(zmax).value

        zmid = 0.5*(zmax+zmin)
        dmid = Planck15.luminosity_distance(zmid).value
        while zmax-zmin > rtol*zmid:
            if dmid == event.distance:
                break
            elif dmid > event.distance:
                zmax = zmid
                dmax = dmid
            else: ### dmid < event.distance
                zmin = zmid
                dmin = dmid
            zmid = 0.5*(zmax+zmin)
            dmid = Planck15.luminosity_distance(zmid).value

        setattr(event, 'z', zmid)

class ComponentMass2MchirpEta(AttributeTransformation):
    """Compute chirp mass and symmetric mass ratio from component masses
    """
    _required = ('mass1', 'mass2')
    _variates = ('Mc', 'eta')

    @staticmethod
    def transform(event):
        setattr(event, 'Mc', (event.mass1*event.mass2)**(3./5.)*(event.mass1+event.mass2)**(-1./5.))
        setattr(event, 'eta', event.mass1*event.mass2/(event.mass1+event.mass2)**2)

class ComponentMass2q(AttributeTransformation):
    """compute mass ratio based on component masses
    """
    _required = ('mass1', 'mass2')
    _variates = ('q',)

    @staticmethod
    def transform(event):
        setattr(event, 'q', min(event.mass1, event.mass2) / max(event.mass1, event.mass2))

class SourceMass2DetectorMass(AttributeTransformation):
    _required = ('mass1', 'mass2', 'z')
    _variates = ('mass1_source', 'mass2_source', 'Mc_source', 'Mc')

    @staticmethod
    def transform(event):
        setattr(event, 'mass1_source', event.mass1)
        setattr(event, 'mass2_source', event.mass2)
        setattr(event, 'Mc_source', (event.mass1_source*event.mass2_source)**(3./5.)*(event.mass1_source+event.mass2_source)**(-1./5.))

        setattr(event, 'mass1', event.mass1_source * (1 + event.z))
        setattr(event, 'mass2', event.mass2_source * (1 + event.z))
        setattr(event, 'Mc', event.Mc_source * (1 + event.z))

class Spins2Chi(AttributeTransformation):
    _required = ('mass1', 'mass2', 'spin1x', 'spin1y', 'spin1z', 'spin2x', 'spin2y', 'spin2z')
    _variates = ('chi_eff', 'chi_p')

    @staticmethod
    def transform(event):
        setattr(event, 'chi_eff', (event.mass1 * event.spin1z + event.mass2 * event.spin2z) / (event.mass1 + event.mass2))
        s1_ip = np.sqrt(event.spin1x**2 + event.spin1y**2)
        s2_ip = np.sqrt(event.spin2x**2 + event.spin2y**2)
        setattr(event, 'chi_p', (event.mass1 * s1_ip + event.mass2 * s2_ip) / (event.mass1 + event.mass2))

class Spins2ConstantSpinParams(AttributeTransformation):
    """Defined in Farr, et al. 2014 (https://journals.aps.org/prd/abstract/10.1103/PhysRevD.90.024018)
Transformation code stolen from bayespputils (http://software.ligo.org/docs/pylal/pylal.bayespputils-pysrc.html)
"""
    _required = ('mass1', 'mass2', 'spin1x', 'spin1y', 'spin1z', 'spin2x', 'spin2y', 'spin2z', 'inclination', 'coa_phase')
    _variates = (
        "Mc",
        "eta", 
        "theta_jn",
        "phi_jl",
        "theta_1",
        "theta_2",
        "phi_12",
        "phi1",
        "phi2",
        "iota",
        "chi_1",
        "chi_2",
        "spinchi",
        "tilt1",
        "tilt2",
        "beta",
    ) 

    def transform(self, event, f_ref=20.):

        # geomtric utils
        from pylal.bayespputils import \
            cart2sph, sph2cart, \
            array_ang_sep, array_polar_ang, \
            ROTATEZ, ROTATEY

        # physics/PN utils
        from pylal.bayespputils import orbital_momentum

        # FIXME: Hardcoded frame axis
        axis = lalsimulation.SimInspiralGetFrameAxisFromString("OrbitalL")

        m1, m2 = event.mass1, event.mass2
        ComponentMass2McEta.transform(event) ### generates Mc, eta
        mc, eta = event.Mc, event.eta

        # Convert to radiation frame
        iota, s1x, s1y, s1z, s2x, s2y, s2z = lalsimulation.SimInspiralInitialConditionsPrecessingApproxs( \
            event.inclination,
            event.spin1x, event.spin1y, event.spin1z, \
            event.spin2x, event.spin2y, event.spin2z, \
            m1*lal.MSUN_SI, m2*lal.MSUN_SI, \
            f_ref, event.coa_phase, axis
        )

        a1, theta1, phi1 = cart2sph(s1x, s1y, s1z)
        a2, theta2, phi2 = cart2sph(s2x, s2y, s2z)

        spins = { \
            'chi_1': a1, 'theta_1': theta1, 'phi_1': phi1,
            'chi_2': a2, 'theta_2': theta2, 'phi_2': phi2,
            'iota': iota,
        }

        # If spins are aligned, save the sign of the z-component
        if event.spin1x == event.spin1y == event.spin2x == event.spin2y == 0.:
            spins['a1z'] = event.spin1z
            spins['a2z'] = event.spin2z

        L  = orbital_momentum(f_ref, m1, m2, iota)
        S1 = np.hstack((s1x, s1y, s1z))
        S2 = np.hstack((s2x, s2y, s2z))

        zhat = np.array([0., 0., 1.])
        aligned_comp_spin1 = np.dot(S1, zhat)
        aligned_comp_spin2 = np.dot(S2, zhat)
        chi = aligned_comp_spin1 + aligned_comp_spin2 + \
              np.sqrt(1. - 4. * eta) * (aligned_comp_spin1 - aligned_comp_spin2)

        spins['spinchi'] = chi

        S1 *= m1**2
        S2 *= m2**2
        J = L + S1 + S2

        tilt1 = array_ang_sep(L, S1)
        tilt2 = array_ang_sep(L, S2)
        beta  = array_ang_sep(J, L)

        spins['tilt1'] = tilt1
        spins['tilt2'] = tilt2
        spins['beta'] = beta

        # Need to do rotations of XLALSimInspiralTransformPrecessingInitialCondition inverse order to go in
        # the L frame
        # first rotation: bring J in the N-x plane, with negative x component
        phi0 = np.arctan2(J[1], J[0])
        phi0 = np.pi - phi0

        J = ROTATEZ(phi0, J[0], J[1], J[2])
        L = ROTATEZ(phi0, L[0], L[1], L[2])
        S1 = ROTATEZ(phi0, S1[0], S1[1], S1[2])
        S2 = ROTATEZ(phi0, S2[0], S2[1], S2[2])

        # now J in in the N-x plane and form an angle theta_jn with N, rotate by -theta_jn around y to have J along z
        theta_jn = array_polar_ang(J)
        spins['theta_jn'] = theta_jn

        J = ROTATEY(theta_jn, J[0], J[1], J[2])
        L = ROTATEY(theta_jn, L[0], L[1], L[2])
        S1 = ROTATEY(theta_jn, S1[0], S1[1], S1[2])
        S2 = ROTATEY(theta_jn, S2[0], S2[1], S2[2])

        # J should now be || z and L should have a azimuthal angle phi_jl
        phi_jl = np.arctan2(L[1], L[0])
        phi_jl = np.pi - phi_jl
        spins['phi_jl'] = phi_jl

        # bring L in the Z-X plane, with negative x
        J = ROTATEZ(phi_jl, J[0], J[1], J[2])
        L = ROTATEZ(phi_jl, L[0], L[1], L[2])
        S1 = ROTATEZ(phi_jl, S1[0], S1[1], S1[2])
        S2 = ROTATEZ(phi_jl, S2[0], S2[1], S2[2])

        theta0 = array_polar_ang(L)
        J = ROTATEY(theta0, J[0], J[1], J[2])
        L = ROTATEY(theta0, L[0], L[1], L[2])
        S1 = ROTATEY(theta0, S1[0], S1[1], S1[2])
        S2 = ROTATEY(theta0, S2[0], S2[1], S2[2])

        # The last rotation is useless as it won't change the differenze in spins'
        # azimuthal angles
        phi1 = np.arctan2(S1[1], S1[0])
        phi2 = np.arctan2(S2[1], S2[0])
        if phi2 < phi1:
            phi12 = phi2 - phi1 + 2. * np.pi
        else:
            phi12 = phi2 - phi1
        spins['phi_12'] = phi12

        ### actually save the results
        for k in self._variates:
            if k in spins: ### necessary because we also generate Mc, eta...
                setattr(event, k, spins[k])

def EMBright(AttributeTransformation):
    """associated probability of being EMBright based on masses, spins, etc
    """
    _required = ('mass1', 'mass2', 'chi_eff')
    _variates = ('disk_mass',)

    @staticmethod
    def transform(event):
        from gw_event_gen.em_bright import frac_disk_mass
        setattr(event, 'disk_mass', max(frac_disk_mass(event.mass1, event.mass2, event.chi_eff), 0.))

class SNR(AttributeTransformation):
    """associate the SNRs for this event
    """
    _required = Event._required ### we need waveforms and location on the sky

    def __init__(self, network):
        self._network = network

    @property
    def _variates(self):
        return self._SNR_tuple()

    def _SNR_tuple(self):
        return tuple(self._snr_name(i) for i in self._network._instr) + (self._snr_name('net'),)

    @staticmethod
    def _snr_name(i):
        return '%s_snr'%i

    def transform(self, event):
        snrs = self._network.snr(event)
        for i in self._network._instr:
            setattr(event, self._snr_name(i), snrs[i])
        setattr(event, self._snr_name('net'), np.sqrt(sum(np.asarray(list(snrs.values()))**2)))

class Pdet(AttributeTransformation):
    """associate the probability of detecting this event based on a non-central chi2 distribution
    """
    _required = ('net_snr',)
    _variates = ('pdet',)

    def __init__(self, snr_thr=8.):
        self._snr_thr2 = snr_thr**2

    def transform(self, event):
        snr_net2 = event.net_snr**2
        setattr(event, 'pdet', 1. - ncx2.cdf(self._snr_thr2, 2, snr_net2)) ### the survival function, which is 1-cdf
