# General Information

The structure of an `eventgen.EventGenerator` object usually proceeds in three phases:
  * `EventGenerator.append_generator`
  * optional: `EventGenerator.append_post`
  * optional: `EventGenerator.append_conditional`

Each of these is called in the order above for each event generated.

## Generation

Example:

```python
generator.append_generator(("mass1", "mass2"), eventgen.uniform_comp_mass, \
			min_primary = 1.0, max_primary = 10.0,
			min_secondary = 1.0, max_secondary = 10.0)
```

In general, you add the attribute labels (the masses), the function that generates them (`eventgen.uniform_comp_mass`), and optionally, any `kwargs` to hand to that function (which will be bound on call).

## Post

```python
generator.append_post(("mchirp", "eta"), mchirp_eta)
```

This follows in the same way as the generators, but is called *after* the generators (and thus depends on them). They are called in the order they are added, i f one post depends on another. This used to define attributes which may be useful later on, but are not randomized intrinsically.

Note that functions handed to this appender take only one argument: the event itself (which will have all the attributes generated so far defined).

## Conditional

```python
generator.append_post(("snr",), lambda e: generate_snr(e))
generator.append_conditional(lambda e: e.snr > 8)
```

Conditionals are a manner of rejection sampling. If defined, they are called in order and should be provided a function of one argument (as in the post stage) and return a boolean value. Those not passing the test are rejected. For the full implementation of the above SNR cut, see the examples below.

## Preliminaries

```python
import sys
import os

from functools import partial

import numpy

from astropy import units
from astropy.cosmology import Planck15, z_at_value

import eventgen
from netsim import Network
```

## Specific settings

```
n_events = 10
snr_thresh = 4.
```

```python
# FIXME: Move to eventgen
def delta_fcn_comp_mass(mass1=10., mass2=10.):
    """
    Generate mass pairs with specific values.
    """
    return mass1, mass2

# FIXME: Use eventgen's version once 2g_localization is merged to master
def astro_bbh_comp_mass(min_bh_mass, max_bh_mass, max_mtotal=100, \
                            min_q=0.1, max_q=1.0):
    """
    Generate BBH masses. Primary is a power law (-2.3) in 5 - 50 and the secondary is generated according to a uniform distribution in mass ratio such that min_q < q < m_max_q.
    """
    assert 0 < min_q <= max_q
    assert min_q <= max_q <= 1
    m1, _ = eventgen.astro_comp_mass(min_primary=min_bh_mass, \
                                        max_primary=max_bh_mass)
    m2 = numpy.random.uniform(min_q, max_q) * m1
    assert m1 + m2 <= max_mtotal
    return m1, m2
```

## Build Up Network

Without a second argument, the default 2G PSD is used.

```python
aligo = Network()
aligo.add_psd("H1")

# Enable to make 3-ifo network
#aligo.add_psd("L1")
#aligo.add_psd("V1")
```

## Check horizons

```
print aligo.horizon(mass1=10., mass2=10.)
zmax_10_10 = aligo.redshift_horizon(mass1=10., mass2=10., \
                                        snr_threshold=snr_thresh)["H1"]
print zmax_10_10, Planck15.luminosity_distance(zmax_10_10)

zmax_50_50 = aligo.redshift_horizon(mass1=50., mass2=50., \
                                        snr_threshold=snr_thresh)["H1"]
print zmax_50_50, Planck15.luminosity_distance(zmax_50_50)
```

## Speed Up the Redshift Generation

```python
#
# Generate look up table
#
mass_grid = numpy.linspace(5, 50., 100)
lookup_table = [aligo.redshift_horizon(mass1=m, mass2=m, \
                                        snr_threshold=snr_thresh)["H1"] \
                                            for m in mass_grid]
# Grid is in total mass
mass_grid += mass_grid

# FIXME: Use eventgen's version once 2g_localization is merged to master
def astro_bbh_comp_mass_and_z(min_bh_mass, max_bh_mass, max_mtotal=100, \
                                min_q=0.1, max_q=1.0, z_min=0):
    """
    Draw masses according to `astro_bbh_comp_mass`, and then, based on a predefined lookup table, determine a z_max with which to appropriately draw a redshift from. If specified, z_min can also be used.
    """

    # Draw new masses
    m1, m2 = astro_bbh_comp_mass(min_bh_mass, max_bh_mass, max_mtotal, \
                                    min_q, max_q)

    # Draw redshift based on masses
    mtot = m1 + m2
    mtot_idx = numpy.searchsorted(mass_grid, mtot, side='right')
    new_zmax = lookup_table[mtot_idx]

    #assert new_zmax > aligo.redshift_horizon(mass1=m1, mass2=m2, \
                                                #snr_threshold=snr_thresh)["H1"]
    return m1, m2, eventgen.uniform_comoving_redshift(new_zmax, z_min)[0]
```

## Setup a "Default" Generator

If you find yourself using the same "pieces" over and over again, you can generate a "default" generator with those pieces in place. For instance, the orientation and position parameters are usually isoptropic, and the eccentricities usually zero. This is encompassed here.

```python
def default_generator():
    generator = eventgen.EventGenerator()

    generator.append_generator(eventgen._spins_cart, eventgen.zero_spin)
    generator.append_generator(("eccentricity",), lambda: (0.0,))

    generator.append_generator(eventgen._extr, eventgen.random_orientation)
    # FIXME: Randomize this
    generator.append_generator(("event_time",), lambda: (1e9,))

    generator.append_conditional(lambda e: e.snr > snr_thresh)

    return generator
```
## Different Populations

### 10 + 10, no redshift

```python
generator = default_generator()
generator.append_generator(("mass1", "mass2"), delta_fcn_comp_mass)

# Uniform to 3 Gpc
generator.append_generator(("distance",), eventgen.uniform_volume, d_max=3e3)
generator.append_post(("snr",), lambda e: (aligo.snr(e)["H1"],))

for event in generator.generate_events(n_itr=n_events):
    #print event.snr
    pass

generator.to_hdf5(output_file, root="no_redshift_10_10", \
			verbose=True)
```

### astro (power law) BBH, no redshift

```python
generator = default_generator()
generator.append_generator(("mass1", "mass2"), astro_bbh_comp_mass, \
				min_bh_mass=5., max_bh_mass=50.)

# Uniform to 3 Gpc
generator.append_generator(("distance",), eventgen.uniform_volume, d_max=3e3)
generator.append_post(("snr",), lambda e: (aligo.snr(e)["H1"],))

for event in generator.generate_events(n_itr=n_events):
    #print event.snr
    pass

generator.to_hdf5(output_file, root="no_redshift_astro_bbh", \
			verbose=True)
```

### 10 + 10, with redshift

```python
generator = default_generator()
generator.append_generator(("mass1", "mass2"), delta_fcn_comp_mass)

# Uniform to 3 Gpc
generator.append_generator(("distance",), eventgen.uniform_volume, d_max=3e3)

# Get equipped with redshift
generator.append_post(("z",), \
lambda e: (z_at_value(Planck15.luminosity_distance, e.distance * units.Mpc),))
generator.append_post(eventgen._redshifted_masses, eventgen.detector_masses)
generator.append_post(("snr",), lambda e: (aligo.snr(e)["H1"],))

for event in generator.generate_events(n_itr=n_events):
   #print event.snr
   pass

generator.to_hdf5(output_file, root="euclid_redshift_10_10", \
			verbose=True)
```

### astro (power law) BBH, with redshift

```python
generator = default_generator()
generator.append_generator(("mass1", "mass2"), astro_bbh_comp_mass, \
				min_bh_mass=5., max_bh_mass=50.)

# Uniform to 3 Gpc
generator.append_generator(("distance",), eventgen.uniform_volume, d_max=3e3)

# Get equipped with redshift
generator.append_post(("z",), \
    lambda e: (z_at_value(Planck15.luminosity_distance, e.distance * units.Mpc),))
generator.append_post(eventgen._redshifted_masses, eventgen.detector_masses)
generator.append_post(("snr",), lambda e: (aligo.snr(e)["H1"],))

for event in generator.generate_events(n_itr=n_events):
   #print event.snr
   pass

generator.to_hdf5(output_file, root="euclid_redshift_astro_bbh", \
			verbose=True)
```

### 10 + 10, uniform in comoving volume

```python
generator = default_generator()
generator.append_generator(("mass1", "mass2"), delta_fcn_comp_mass)

generator.append_generator(("z",), eventgen.uniform_comoving_redshift, \
			z_max=zmax_10_10)

generator.append_post(("distance",), eventgen.uniform_luminosity_dist)
generator.append_post(eventgen._redshifted_masses, eventgen.detector_masses)
generator.append_post(("snr",), lambda e: (aligo.snr(e)["H1"],))

for event in generator.generate_events(n_itr=n_events):
    #print event.snr
    pass

generator.to_hdf5(output_file, root="redshift_10_10", \
			verbose=True)
```

### astro (power law) BBH, uniform in comoving volume

```python
generator = default_generator()
generator.append_generator(("mass1", "mass2", "z"), \
			astro_bbh_comp_mass_and_z, \
			min_bh_mass=5., max_bh_mass=50.)

generator.append_post(("distance",), eventgen.uniform_luminosity_dist)
generator.append_post(eventgen._redshifted_masses, eventgen.detector_masses)
generator.append_post(("snr",), lambda e: (aligo.snr(e)["H1"],))

for event in generator.generate_events(n_itr=n_events):
    #print event.snr
    pass

generator.to_hdf5(output_file, root="redshift_astro_bbh", \
			verbose=True)
```
